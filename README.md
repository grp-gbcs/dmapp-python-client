# DMApp Python Client

This repo contains a client module as interface to the STOCKS API. 

## Quick Start

To install, either use our gitlab pypi repository or clone the repo

### Install

**Optional**: create a virtual/conda env 
```bash 
conda create -n dmapp-client python=3 && conda activate dmapp-client
```

#### Gitlab PyPi 

You will have to get your `<personal_token>` from the gitlab settings

```bash
pip install DMApp-client --extra-index-url https://__token__:<your_personal_token>@git.embl.de/api/v4/projects/4166/packages/pypi/simple
```

#### Clone the repo and install

```bash 
git clone https://git.embl.de/grp-gbcs/dmapp-python-client.git
cd dmapp-python-client
pip install -e .
```
