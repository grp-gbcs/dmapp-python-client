# -*- coding: utf-8 -*-
import json
import logging
from typing import Literal, NoReturn, Dict, Tuple, Union, Optional
from urllib.parse import urljoin

import requests

logger = logging.getLogger(__name__)


class AuthenticationError(Exception):
    def __init__(self, message, status_code=None):
        super().__init__(message)
        self.status_code = status_code

    def __str__(self):
        if self.status_code:
            return f"{self.status_code}: {self.args[0]}"
        return str(self.args[0])


def handle_response(response) -> Tuple[int, Union[Dict, str]]:
    logger.debug(response.status_code)
    logger.debug(response.content)
    try:
        return response.status_code, response.json()
    except json.JSONDecodeError:
        logger.debug("Could not serialize server response.")
        logger.debug(response.__dict__)
        return response.status_code, response.content


ACTION_STATUS = Literal["INPROGRESS", "SUCCESS", "FAILURE", "UNKNOWN"]


class DMAppClient:
    def __init__(self, base_url: str, username: str, scope: str, password: Optional[str] = None,
                 token: Optional[str] = None):
        self.base_url = base_url if base_url.endswith("/") else base_url + "/"
        self.host = base_url[:-1]
        self.username = username
        self.scope = scope
        if password is None and token is None:
            raise ValueError("Need password or token")
        self.password = password
        self.token = token

    def authenticate(self) -> NoReturn:
        if self.token:
            # test that we are authenticated
            response = requests.get(urljoin(self.base_url, "projects"))
            if response.ok:
                # token valid and no need to authenticate
                return
            elif self.password is None:
                raise AuthenticationError("Token invalid and no password provided to reauthenticate.")
        response = requests.post(urljoin(self.base_url, "login"),
                                 data={"username": self.username, "password": self.password,
                                       "scope": self.scope}
                                 )
        if response.ok:
            self.token = response.text
        elif response.status_code == 401:
            raise AuthenticationError(f"Username and password combination not accepted: {response.content}",
                                      status_code=response.status_code)

    def _get_headers(self, **kwargs) -> Dict:
        return {"Authorization": f"Bearer {self.token}"}

    def get(self, path: str, query_params: dict = None) -> tuple:
        url = urljoin(self.base_url, path)
        logger.debug("Fetching url: %s", url)
        logger.debug("With query params: %s", query_params)
        response = requests.get(url, headers=self._get_headers(), params=query_params)
        return handle_response(response)

    def post(self, path, payload, query_params=None):
        url = urljoin(self.base_url, path)
        logger.debug("POST to url: %s", url)
        logger.debug("With query params: %s", query_params)
        logger.debug("Payload: %s", payload)
        response = requests.post(url,
                                 json=payload,
                                 headers=self._get_headers(),
                                 params=query_params)
        return handle_response(response)

    def get_action_status(self, action_id: str) -> ACTION_STATUS:
        """
        GET /api/v1/actions/{action_id}/status
        """
        status_code, ret = self.get(f"actions/{action_id}/status")
        if status_code == 200:
            return ret
        return "UNKNOWN"

    def archive_collection(self, collection_id, action_data):
        """
        POST /api/v1/collections/{collection_id}/archive
            {action_data}
        """
        action_data["type"] = "ARCHIVE"
        return self.post(f"collections/{collection_id}/archive", action_data)

    def register_collection(self, name, comment=""):
        return self.post("collections", {"name": name, "comment": comment})

    def register_dataset(self, uri, name, dataset_type, collection_id, comment=""):
        return self.post("data", payload={"path": uri, "name": name, "type": dataset_type,
                                          "collectionId": collection_id, "comment": comment})

    def get_dataset(self, dma_id):
        return self.get(f"data/{dma_id}")

    def get_user_data(self, user, query_params=None):
        if query_params is None:
            query_params = {}
        query_params["user"] = user
        return self.get("data/user", query_params=query_params)

    def get_dma_id_for_path(self, file_path: str, user=None) -> int:
        if user is None:
            user = self.username
        data = self.get_user_data(user, {"pathContains": file_path})
        if len(data) > 1:
            raise ValueError(f"Got more than one dataset for '{file_path}'")
        elif len(data) == 0:
            raise ValueError(f"No dataset found for '{file_path}'")
        return data[0]["id"]

    def delete(self, dma_id):
        """
        Request delete action for data specified by ID
        """
        return self.post(f"data/{dma_id}/delete", payload=None)

    def handover(self, dma_id, path, owner, name=None, collection_id=None, delete_original=False, comment=None) -> \
        Tuple[int, dict]:
        """
        POST /api/v1/data/{dma_id}/handover
        {
          "comment": "string",
          "copyTo": "string",
          "deleteOriginal": true,
          "setCollection": 0,
          "setName": "string",
          "setOwner": "string"
        }
        """
        action_data = {
            "comment": comment,
            "copyTo": path,
            "delete_original": delete_original,
            "setCollection": collection_id,
            "setName": name,
            "setOwner": owner
        }
        return self.post(f"data/{dma_id}/handover", action_data)
