from setuptools import find_packages, setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="DMApp-client",
    version="0.0.2",
    packages=["dmapp_client", "."],
    include_package_data=True,
    # entry_points="""
    #     [console_scripts]
    #     stocks=stocks:stocks
    # """,
    install_requires=["Click>=7,<8", "pyyaml>=5,<6", "requests>=2,<3", "pyjwt>=1.7,<2"],

    package_data={
        # If any package contains *.txt or *.rst files, include them:
        "": ["*.md"],
    },

    # metadata to display on PyPI
    author="Jelle Scholtalbers",
    author_email="j.scholtalbers@gmail.com",
    description="A client to interface with the EMBL DMApp API.",
    keywords="EMBL DMApp",
    url="https://git.embl.de/grp-gbcs/dmapp-python-client",  # project home page, if any
    project_urls={
        "Bug Tracker": "https://git.embl.de/grp-gbcs/dmapp-python-client",
        "Documentation": "https://git.embl.de/grp-gbcs/dmapp-python-client",
        "Source Code": "https://git.embl.de/grp-gbcs/dmapp-python-client",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License"
    ]
)
